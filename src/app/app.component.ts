import { Component } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HubConnectionManager } from './helpers/hub-connection-manager';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private object: Subject<any>;
  private urlConnnection: string = 'http://channelapplication20190117042943.azurewebsites.net/channelHub';
  private channels: any[] = [];
  private sub: Observable<any>;
  private disableButtons: boolean = false;
  private shows: any[] = [];

  constructor(private hubConnectionManager: HubConnectionManager) {
    this.hubConnectionManager = new HubConnectionManager();
  }
  ngOnInit() {
    this.hubConnectionManager.createHubConnection(this.urlConnnection, true);

    //subscribe to connectSubject to know when is connected
    this.hubConnectionManager.connectSubject.subscribe(res => {
      if (res === true) {
        this.hubConnectionManager.invoke('GetChannels');
      }
    });

    //Callback to getChannels method
    this.hubConnectionManager.on('getChannels', (_channels: any) => {
      this.channels = _channels;
    });
  }

  requestShows(channel_id: number) {
    this.shows = [];
    this.disableButtons = true;
    this.sub = new Subject();
    //if the connectionManager is Connected, get the shows stream
    if (this.hubConnectionManager.isConnected()) {
      this.hubConnectionManager.stream("GetShows", channel_id, this.sub);
    }

    //Subscribe to the stream and emit shows to an event
    this.sub.subscribe(res => {
      this.shows = [...this.shows, res];
    }, err => {
      console.log(err);
    }, () => {
      //disable buttons when finished
      this.disableButtons = false;
    });
  }
}
