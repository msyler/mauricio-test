import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChannelListComponent } from './components/channel-list/channel-list.component';
import { ShowListComponent } from './components/show-list/show-list.component';
import { HubConnectionManager } from './helpers/hub-connection-manager';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ChannelListComponent,
    ShowListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [HubConnectionManager],
  bootstrap: [AppComponent]
})
export class AppModule { }
