import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShowListComponent implements OnInit {

  //@Input() newShow: string;
  @Input() newShow: any[];
  constructor() { }

  ngOnInit() {
  }

  //Check for changes in the input to reset and show the shows
  ngOnChanges(changes: SimpleChanges) {
    
  }
}

