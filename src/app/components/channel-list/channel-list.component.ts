import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { Subject } from "rxjs";

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.scss']
})
export class ChannelListComponent implements OnInit {
  //URL CONNECTION
  @Input() channels: any[];
  @Input() disableButtons: boolean = false;
  @Output() requestShowsEvent = new EventEmitter();

  constructor() {
   
  }

  ngOnInit() {
    
  }

  requestShows(channel_id) {
    this.requestShowsEvent.emit(channel_id);
  }
}
